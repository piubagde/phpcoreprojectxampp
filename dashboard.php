<html>
<style>
	form.example input[type=text] {
	  padding: 10px;
	  font-size: 17px;
	  border: 1px solid grey;
	  float: left;
	  width: 80%;
	  background: lightgrey;
	}

	form.example input[type=button] {
	  align: center;
	  width: 20%;
	  padding: 10px;
	  background: #2196F3;
	  color: white;
	  font-size: 17px;
	  border: 1px solid grey;
	  border-left: none;
	  cursor: pointer;
	}

	.pagination a {
		color: black;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color.red;
        }

	.pagination a.active {
    	background-color: dodgerblue;
        color: white;
	}

	.pagination a:hover:not(.active) {
    	background-color: lightgrey;
	}

</style>

<body>
<br>
<?php
	session_start();
	if(isset($_SESSION["user_name"]) && $_SESSION["user_name"]!=""){
		echo "Hello ".$_SESSION["user_name"];
        } else {
			header ("Location: login_form.php");	
		}

	//connecting with database admineusers
	function dbConnect(){
		$connection = mysqli_connect("localhost", "root", "", "adminusers");

        if( $connection->connect_error ) {
                echo "Connection Failed: " . mysqli_connect_error();
		} else{
			return $connection;
		} 
	}
	$conct = dbConnect();
	
	$where="";
	if(isset($_REQUEST['city']) && $_REQUEST['city']!=""){		
			$where = $where . " AND `city` = '$_REQUEST[city]'";	
	}
	
	if(isset($_REQUEST['name']) && $_REQUEST['name']!=""){

			$where = $where . " AND `name` = '$_REQUEST[name]'";	
	
	}

	$totalrecords_sql = "SELECT count(*) AS count FROM `users` WHERE 1=1 $where";
	$totalrecords_res = $conct->query($totalrecords_sql);
//	error_log("\n===var = $totalrecords ===\n");
//	echo $totalrecords_sql;

	$total = $totalrecords_res->fetch_assoc();
	$totalrecords = $total["count"];
	error_log("\n===" . $totalrecords . "===\n");
//	$totalpage = ceil($totalrecords/$limit);
	
	$offset_limit="";
	$limit="10";

	if(isset($_REQUEST['offset']) && isset($_REQUEST['limit']) && $_REQUEST['offset']!="" && $_REQUEST['limit']!=""){
	
		$offset_limit = " LIMIT $_REQUEST[offset],$_REQUEST[limit]";
		$limit = $_REQUEST['limit'];
	}


	$select_sql = "SELECT * FROM `users` WHERE 1=1 $where ORDER BY `id` ASC $offset_limit";
	
//	echo $select_sql;

	$res = $conct->query($select_sql);
//	echo "<pre>";
//		var_dump($res);


?>
<br>
<div> <a>
<div style="align:center; white-space: nowrap;">
	<div align="left">
		<b>Dashboard</b>
	</div>
	<div>
	    <a style="display:inline-block; float:left" href="logout.php">Logout</a>
	</div>
<br>
	<div>
		<a href='add_form.php'>Add</a>
<div>
<br>

<!-- search box for filter -->
<form class="search" action="dashboard.php" style="margin:auto;max-width:300px">
  <input type="text" placeholder="Search by city..." name="city"><br>
<br>
  <input type="text" placeholder="Search by name..." name="name"><br>
<br>
  <input type="submit" value="search">
</form>
<br><br>

</div align="center">
	<form action="importcsv.php" method="post" enctype="multipart/form-data">
		
		<input type="file" name="upload"/>
		<input type="submit" value="Save"/>
		
	</form>
</div>

<!-- getting data from database and dispalying in form of table -->
<br>

<div align="center">
<a href="exportcsv.php">Export Csv</a>
</div>

<br>
<div align="center">
<table border=1px>	
	<tr>
		<th>Sr.No.</th>
		<th>Name</th>
		<th>Age</th>
		<th>City</th>
		<th>Image</th>
		<th colspan=2>Action</td>
	</tr>
<?php

	while($row = $res->fetch_assoc()){
	
?>
		
	<tr>
		<td><?php echo $row["id"]; ?></td>
		<td><?php echo $row["name"];?></td>
		<td><?php echo $row["age"];?></td>
		<td><?php echo $row["city"];?></td>
		<td><img src="<?php echo $row["image_url"] ?>" width=100 height=100/></td>
		<td><a href='edit_form.php?id=<?php echo $row["id"];?>&amp; name=<?php echo $row["name"];?>&amp; age=<?php echo $row["age"];?>&amp;city=<?php echo $row["city"];?>'>edit</a></td>
		<td><a href='delete_action.php?id=<?php echo $row["id"];?>' onclick="javascript: return confirm('Are you sure you want to delete this Entry?')">Delete</a></td>
	</tr>
<?php
	} 
?>
</table>
</div>
<br>
	<hr>
	
	<?php
		$pages_count =ceil($totalrecords/$limit);
		
		for($i=0; $i<$pages_count; $i++){
			$offset = $i*$limit;
	?>
		
		<a href="dashboard.php?offset=<?php echo $offset; ?>&limit=<?php echo $limit;?>"> <?php echo $i+1; ?> </a>&nbsp;
				
	<?php
		}

	?>

	
</body>
</html>

