<html>
	<style>
		/* Bordered form */
		form {
			border: 2px solid #f1f1f1;
			margin: 2px;
			padding: 8px;
		}
		
		input[type=text], input[type=password] {
			padding: 2px;
			box-sizing: border-box;
			font-size: 6px
		}

		.sameline {
			white-space: nowrap;
			padding: 3px;
			font-size: 8px;
		}
		
		input[type="submit"] {
			width: 15%;
			margin: 2px, 0px;
			padding: 2px;
			box-sizing: border-box;
			border: 2px, solid;
			border-radius: 3px;
			font-size: 6px;
			font-family: Helvetica;
		}

		#forgotpwd {
			font-size:6px;
			padding-top:5px;
		}
	</style>

<body>

<?php
	if(isset($_REQUEST["msg"]) && $_REQUEST["msg"]!=""){	
		echo $_REQUEST["msg"];
	}
	session_start();
	if(isset($_SESSION["user_name"]) && $_SESSION["user_name"]!=""){
		header ("Location: dashboard.php");
	}
?>
<div align="center">	
	<form action="login_action.php" method="post">
		<div style="font-size: 12px"><b> Login Form </b></div>
		<div class="sameline">
			<label for="uname"><b> Username </b></label>
			<input type="text" placeholder="enter username here" name="uname" required/>
		</div>
		<div class="sameline">
	        <label for="pwd"><b> Password </b></label>
	        <input type="password" name="pwd" required/>
		</div>
		<div>
			<input type="submit" value="Login"/>
			<input type="submit" value="Signup"/>
			<br>
		</div>
		<div id="forgotpwd">
			<span><a href="#">Forgot Password</a></span>
		</div>
	</form>
</div>
</body>
</html>
