<?php

header('Content-type: application/json');

require("../functions.php");
$conct = getConn();

if ($_SERVER["REQUEST_METHOD"] == "DELETE"){

	//takes raw data from request
	$json = file_get_contents('php://input');
	
	//converts request into php object
	$data = json_decode($json);
		
	$id = $data->id;
	
	$select_sql = "DELETE FROM `users` WHERE id = $id LIMIT 1";
	
	$res = $conct->query($select_sql);

	echo json_encode(["status" => true, "msg" => "record deleted"]);
	
} else {
	
	echo json_encode(["status" => false, "msg" => "invalid method"]);
	}

?>
