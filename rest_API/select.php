<?php

header('Content-type: application/json');

require("../functions.php");
$conct = getConn();
require("JWT/src/JWT.php");

$key = "testkey";

$jwt = $_SERVER["HTTP_JWT"];

$decoded = JWT::decode($jwt, $key, array('HS256'));

$select_sql = "SELECT * FROM `users`";
	
$res = $conct->query($select_sql);

if (($_SERVER["PHP_AUTH_USER"] == "admin" && $_SERVER["PHP_AUTH_PW"] == "@123") || 
	$decoded->user == "admin" && $decoded->pwd == "@123"){
		
	if (mysqli_num_rows($res) > 0){
		// whileloop to get all data
		// while($row = $res->fetch_assoc()){ 
		$row = mysqli_fetch_assoc($res);
//		print_r($row);
//	}
		if ($_SERVER["REQUEST_METHOD"] == "GET"){
			echo json_encode(["status" => true, "data" => $row]);
	
		} else {
			echo json_encode(["status" => false, "msg" => "invalid method"]);
		
		}
	} else {
	
		echo json_encode(["status" => false, "msg" => "No records found"]);
	}

} else {
	
	echo json_encode(["status" => false, "msg" => "unauthorized user"]);
}


?>
